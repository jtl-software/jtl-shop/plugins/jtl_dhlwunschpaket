## DHL-Wunschzustellung-Plugin für JTL-Shop

####  Version 2.2.1
- Behebt einen Validierungsfehler, wenn keine Lieferadressvorlage ausgewählt ist

#### Version 2.2.0
- Plugin wurde umbenannt von DHL-Wunschpaket zu DHL-Wunschzustellung
- Kompatibilität zu JTL-Shop 5.2.0 hergestellt
- Anbindung an neue Location Finder API
- Behebt Fehler im Zusammenspiel mit dem PayPal Checkout Plugin
- Behebt eine falsche Versandklassen Prüfung
- weitere kleine Bugfixes

#### Version 2.1.1
- Migration auf Location API Unified

#### Version 2.0.1 
- Fehler: Post-Nummer ist nur beim aktiviertem Adresszusatz-Feld verfügbar (SHOP-5460)
- Fehler: Filialsuche funktioniert nicht unter php 8.0 (SHOP-5661)
- Fehler: Button "Postfiliale Suchen" wird nach Änderung der Adresse nicht wieder eingeblendet (SHOP-4310)
- Post-Nummer ist nur beim aktiviertem Adresszusatz-Feld verfügbar (SHOP-5460)
- Bei Lieferung an Postfiliale auch Adresszusatz als "Postnummer" anzeigen (SHOP-5550)
- Validierung von Packstation- und Post-Nummer (SHOP-5461)

#### Version 2.0.0 
- initiales Release
